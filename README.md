**Running The Project**

* Run mvn clean package to build your application
* Start application with java -jar target/shooter-game-1.0-SNAPSHOT.jar server config.yml
* To check that your application is running enter url http://localhost:8080
