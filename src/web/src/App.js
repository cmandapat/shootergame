import React, { Component } from 'react';
import Websocket from 'react-websocket';
import logo from './logo.svg';
import './App.css';

var connection;
class App extends Component {

   webSocketConnection() {
        
        window.WebSocket = window.WebSocket || window.MozWebSocket;
        connection = new WebSocket('ws://127.0.0.1:8087');

        connection.onopen = function () {
            console.log("connection successful");
        };
        connection.onerror = function(error) {
            console.log("connection fail",error);
        };
        connection.onmessage= function(message) {
          var canvas = document.getElementById("myCanvas");                    
          var ctx = canvas.getContext("2d");
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          ctx.fillStyle = 'green';
          ctx.fillRect(0, 0, canvas.width, canvas.height);

          return false;
        };

    }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to the Game</h1>
        </header>
          <div className="game">
              <canvas id ="myCanvas" width={800} height={600}>
                  Your browser does not support the canvas element.
              </canvas>
          </div>
          {this.webSocketConnection()}
      </div>
    );
  }
}

export default App;
