package com.jcproduction.game;
import com.jcproduction.game.state.ClientConnection;

import com.jcproduction.game.state.Game;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;




public class Server extends WebSocketServer {

    Map<UUID, ClientConnection> connections = new HashMap<>();
    public static Game GAME;
    final Logger log = LoggerFactory.getLogger(Server.class);

    private static long MAX_ELAPSED_TIME_IN_MS = 20;

    Server(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
//        Optional<UUID> uuidOptional = extractClientId(conn);
//        if(uuidOptional.isPresent()) {
//            UUID connectionIdentifier = uuidOptional.get();
//            connections.put(connectionIdentifier,new ClientConnection(connectionIdentifier,conn));
//
//            log.info("Accepted connection from {} with identifer {}", conn.getRemoteSocketAddress(), connectionIdentifier);
//            log.info("There are " + connections.size() + " clients connected.");
//        }

        conn.send("Welcome to the server!"); //This method sends a message to the new client
        broadcast( "new connection: " + handshake.getResourceDescriptor() ); //This method sends a message to all clients connected
        System.out.println("new connection to " + conn.getRemoteSocketAddress());
    }
    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
        System.out.println("There are " + connections.size() + " clients connected. Closed");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
//        Optional<UUID> identifierOptional = extractClientId(conn);
//        if (identifierOptional.isPresent()) {
//            UUID identifier = identifierOptional.get();
//        }
        System.out.println("received message from "	+ conn.getRemoteSocketAddress() + ": " + message);

    }

    @Override
    public void onMessage( WebSocket conn, ByteBuffer message ) {
        System.out.println("received ByteBuffer from "	+ conn.getRemoteSocketAddress());
    }


    @Override
    public void onError(WebSocket conn, Exception ex) {
        System.err.println("an error has occured on the connection" + conn.getRemoteSocketAddress() + ":" + ex);
    }

    @Override
    public void onStart() {

        System.out.println("server started successfully");
//        Server server = this;
//        new Thread() {
//            @Override
//            public void run() {
//                int iteration = 0;
//                while (true) {
//                    Instant start = Instant.now();
//                    if(iteration <= 150 ) {
//                        connections.entrySet().removeIf(entry->{
//                            UUID identifier = entry.getKey();
//                            if(connections.get(identifier).getWebSocket().isClosed()) {
//                                log.info("Connection for {} is dropped,closing connection:",identifier);
//                                return true;
//                            }
//                            return false;
//                        });
//                        iteration = 0;
////                        log.info("There are {} clients connected.", connections.size());
//
//                        Instant end = Instant.now();
//                        long millisElapsed = end.toEpochMilli() - start.toEpochMilli();
//                        try {
//                            long sleep = MAX_ELAPSED_TIME_IN_MS - millisElapsed;
//                            if (sleep <= 0) {
//                                log.warn("Processing took a really long time! Not sleeping! {}", sleep);
//                            } else {
//                                Thread.sleep(sleep); // Sleep for 20ms overall
//                            }
//                            iteration++;
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        }.start();
    }



    /**
     * Extract request identifier from a connection.
     *
     * Use method with an incoming mesage to identify who it is from.
     * @param conn the incoming connection message
     * @return {@link UUID} if success. {@link Optional#EMPTY} if we cannot
     * */
    private Optional<UUID> extractClientId(WebSocket conn) {
        if (conn == null ) {
            return Optional.empty();
        }

        String resourceDescriptor = conn.getResourceDescriptor();
        if(resourceDescriptor == null || resourceDescriptor.length() == 0) {
            return Optional.empty();
        }
        try {
            return Optional.of(UUID.fromString(resourceDescriptor.substring(resourceDescriptor.lastIndexOf("/")+1)));
        } catch(IllegalArgumentException e) {
            conn.close();
            return Optional.empty();
        }
    }
}
