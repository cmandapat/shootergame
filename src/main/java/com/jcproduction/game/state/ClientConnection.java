package com.jcproduction.game.state;

import lombok.Data;
import org.java_websocket.WebSocket;

import java.util.UUID;
@Data
public class ClientConnection {
    private UUID uuid;
    private WebSocket webSocket;

    public ClientConnection(UUID uuid, WebSocket webSocket) {
        this.uuid = uuid;
        this.webSocket = webSocket;
    }
}
