package com.jcproduction.game.state;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Game {

    /**
     * Stores a player's requested move for the next server tick.
     *
     * @param identifier Identifying {@link UUID} for a {@link Player}
     * @param message A {@link Player}'s requested move. For ex: 'WD' for someone wanting to move up and right.
     */
//    public void processMessage(UUID identifier, String message) {
//        if (message == null) {
//            return;
//        }
//
//        log.debug("{} wants to make move: {}", identifier, message);
//        Player player = players.get(identifier);
//        if (player == null) {
//            log.warn("Requested move for a player that doesn't exist. Is {} still active?", identifier);
//            return;
//        }
//
//        // Store all movement commands 'WASD' to be processed on server tick
//        // Process action commands immediately
//        String[] requestedMoves = message.split("");
//
//        Set<String> requestedMovesSet = new HashSet<>();
//        Set<String> requestedActionMoves = new HashSet<>();
//        for (String requestedMove : requestedMoves) {
//            if (requestedMove.equals("A") || requestedMove.equals("D") || requestedMove.equals("S") || requestedMove.equals("W")) {
//                requestedMovesSet.add(requestedMove);
//            } else {
//                requestedActionMoves.add(requestedMove);
//            }
//        }
//
//        player.setRequestedMoves(requestedMovesSet);
//        processRequestedAction(player, requestedActionMoves);
//    }
}
