package com.jcproduction.game;

import com.jcproduction.game.resources.SystemInfo;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.java_websocket.WebSocket;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;

public class GameApplication extends Application<GameConfiguration> {
    public static void main(String[] args) throws Exception{
        new GameApplication().run(args);
        String host = "localhost";
        int port = 8087;
        WebSocketServer server = new Server(new InetSocketAddress(host, port));
        server.run();

    }
    @Override
    public void initialize(final Bootstrap<GameConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/web/assets/", "/", "index.html"));
    }

    @Override
    public void run(final GameConfiguration configuration,
                    final Environment environment) {
        environment.jersey().register(new SystemInfo());
    }
}
